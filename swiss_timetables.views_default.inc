<?php

/**
 * @file
 * Default view with Swiss timetable.
 */

/**
 * Relation's default views.
 */
function swiss_timetables_views_default_views() {
  $views = array();

  $view = new view();
  $view->name = 'swiss_timetables';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'swiss_timetables';
  $view->human_name = 'Swiss timetables';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Swiss timetables';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '10';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '0';
  $handler->display->display_options['pager']['options']['quantity'] = '9';
  $handler->display->display_options['style_plugin'] = 'table';
  /* Field: Swiss timetables: Line nr. */
  $handler->display->display_options['fields']['nr']['id'] = 'nr';
  $handler->display->display_options['fields']['nr']['table'] = 'swiss_timetables';
  $handler->display->display_options['fields']['nr']['field'] = 'nr';
  /* Field: Swiss timetables: Line name */
  $handler->display->display_options['fields']['name']['id'] = 'name';
  $handler->display->display_options['fields']['name']['table'] = 'swiss_timetables';
  $handler->display->display_options['fields']['name']['field'] = 'name';
  /* Field: Swiss timetables: Timetable */
  $handler->display->display_options['fields']['timetable']['id'] = 'timetable';
  $handler->display->display_options['fields']['timetable']['table'] = 'swiss_timetables';
  $handler->display->display_options['fields']['timetable']['field'] = 'timetable';
  $handler->display->display_options['fields']['timetable']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['timetable']['alter']['path'] = '[timetable]';
  $handler->display->display_options['fields']['timetable']['alter']['absolute'] = TRUE;
  $handler->display->display_options['fields']['timetable']['alter']['external'] = TRUE;
  /* Filter criterion: Swiss timetables: Line name */
  $handler->display->display_options['filters']['name']['id'] = 'name';
  $handler->display->display_options['filters']['name']['table'] = 'swiss_timetables';
  $handler->display->display_options['filters']['name']['field'] = 'name';
  $handler->display->display_options['filters']['name']['operator'] = 'contains';
  $handler->display->display_options['filters']['name']['exposed'] = TRUE;
  $handler->display->display_options['filters']['name']['expose']['operator_id'] = 'name_op';
  $handler->display->display_options['filters']['name']['expose']['label'] = 'Line name';
  $handler->display->display_options['filters']['name']['expose']['operator'] = 'name_op';
  $handler->display->display_options['filters']['name']['expose']['identifier'] = 'name';
  $handler->display->display_options['filters']['name']['expose']['required'] = TRUE;
  $handler->display->display_options['filters']['name']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    6 => 0,
    5 => 0,
    4 => 0,
    3 => 0,
  );

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['path'] = 'swiss-timetables';
  $translatables['swiss_timetables'] = array(
    t('Master'),
    t('Swiss timetables'),
    t('more'),
    t('Apply'),
    t('Reset'),
    t('Sort by'),
    t('Asc'),
    t('Desc'),
    t('Items per page'),
    t('- All -'),
    t('Offset'),
    t('« first'),
    t('‹ previous'),
    t('next ›'),
    t('last »'),
    t('Line nr.'),
    t('Line name'),
    t('Timetable'),
    t('Page'),
  );

  $views['swiss_timetables'] = $view;

  return $views;
}
