<?php

/**
 * @file
 * Internal functions for integration with timetable web service.
 */

/**
 * Gets timetable data form cache.
 *
 * @param string $keyword
 *   Search keyword. Defaults to wildcard (all items).
 * @param int $limit
 *   Limit number of results. Defaults to unlimited (NULL).
 * @param int $page
 *   Page number. Defaults to first page (0).
 * @param array $sort
 *   Array of fields to sort on. Field name as key and direction as value.
 *   Direction can be one of SWISS_OFFICIAL_TIMETABLE_SORT_ASC and
 *   SWISS_OFFICIAL_TIMETABLE_SORT_DESC.
 *
 * @return array|false
 *   Data form cache or FALSE if not available.
 */
function _swiss_timetables_cache_get($keyword = '*', $limit = NULL, $page = 0, $sort = array()) {
  $cid = _swiss_timetables_cache_cid($keyword, $limit, $page, $sort);
  if (variable_get('swiss_timetables_cache', TRUE) && $data = cache_get($cid)) {
    return $data->data;
  }

  return FALSE;
}

/**
 * Saves timetable data to cache.
 *
 * @param string $keyword
 *   Search keyword. Defaults to wildcard (all items).
 * @param int $limit
 *   Limit number of results. Defaults to unlimited (NULL).
 * @param int $page
 *   Page number. Defaults to first page (0).
 * @param array $sort
 *   Array of fields to sort on. Field name as key and direction as value.
 *   Direction can be one of SWISS_OFFICIAL_TIMETABLE_SORT_ASC and
 *   SWISS_OFFICIAL_TIMETABLE_SORT_DESC.
 */
function _swiss_timetables_cache_set($data, $keyword = '*', $limit = NULL, $page = 0, $sort = array()) {
  if (variable_get('swiss_timetables_cache', TRUE)) {
    $cid = _swiss_timetables_cache_cid($keyword, $limit, $page, $sort);
    cache_set($cid, $data, 'cache', REQUEST_TIME + variable_get('swiss_timetables_lifetime', 3600));
  }
}

/**
 * Gets cache ID string for timetable data.
 *
 * @param string $keyword
 *   Search keyword. Defaults to wildcard (all items).
 * @param int $limit
 *   Limit number of results. Defaults to unlimited (NULL).
 * @param int $page
 *   Page number. Defaults to first page (0).
 * @param array $sort
 *   Array of fields to sort on. Field name as key and direction as value.
 *   Direction can be one of SWISS_OFFICIAL_TIMETABLE_SORT_ASC and
 *   SWISS_OFFICIAL_TIMETABLE_SORT_DESC.
 *
 * @return string
 *   Cache ID.
 */
function _swiss_timetables_cache_cid($keyword = '*', $limit = NULL, $page = 0, $sort = array()) {
  $cid = "swiss_timetables";
  $cid .= ':' . $keyword . ':' . $page;
  $cid .= $limit ? $limit : 'no-limit';
  foreach ($sort as $sort_field => $sort_direction) {
    $cid .= ':' . $sort_direction == SWISS_TIMETABLES_SORT_DESC ? $sort_field . '-desc' : $sort_field . '-asc';
  }

  return $cid;
}

/**
 * Issues HTTP request and returns timetable data.
 *
 * @param string $keyword
 *   Search keyword. Defaults to wildcard (all items).
 * @param int $limit
 *   Limit number of results. Defaults to unlimited (NULL).
 * @param int $page
 *   Page number. Defaults to first page (0).
 * @param array $sort
 *   Array of fields to sort on. Field name as key and direction as value.
 *   Direction can be one of SWISS_OFFICIAL_TIMETABLE_SORT_ASC and
 *   SWISS_OFFICIAL_TIMETABLE_SORT_DESC.
 *
 * @return array|false
 *   Array of results or FALSE in case of an error. There are two values in the
 *   array. Key total_items stores count of all items (useful for building
 *   pagers). Key data is another array with actual results. Each result is an
 *   object with the following attributes:
 *     - nr: line number
 *     - name: line name
 *     - timetable: URL to PDF timetable
 */
function _swiss_timetables_http_request($keyword = '*', $limit = NULL, $page = 0, $sort = array()) {
  if (!($endpoint = variable_get('swiss_timetables_endpoint'))) {
    return FALSE;
  }
  $result = drupal_http_request(url($endpoint, array(
    'query' => array(
      'type' => 1,
      'search' => $keyword,
    ),
  )));

  // Log any errors.
  if ($result->code != 200) {
    watchdog(
      'swiss_timetables',
      'Search request failed with keyword @keyword, response @code - @response and error message @message.',
      array(
        '@keyword' => $keyword,
        '@code' => $result->code,
        '@response' => $result->status_message,
        '@message' => $result->error,
      ),
      WATCHDOG_ERROR
    );

    return FALSE;
  }

  $data = simplexml_load_string($result->data);

  // Log if we couldn't parse XML.
  if (!$data) {
    watchdog(
      'swiss_timetables',
      'Could not parse XML returned for keyword @keyword.',
      array('@keyword' => $keyword),
      WATCHDOG_ERROR
    );

    return FALSE;
  }

  // Appears JSON functions can nicely decode XML.
  $timetable_data = json_encode($data);
  $timetable_data = json_decode($timetable_data);

  // If only one is found the "API" doesn't return an array but a single element
  // ensure we always have an array.
  if (isset($timetable_data->item) && is_object($timetable_data->item)) {
    $timetable_data->item = array($timetable_data->item);
  }

  if (empty($timetable_data->item)) {
    return array();
  }

  $timetable_data = $timetable_data->item;

  // Sort if needed.
  if ($sort) {
    usort(
      $timetable_data,
      function ($a, $b) use ($sort) {
        $field_name_map = array(
          'nr' => 'fap_nr',
          'name' => 'fap_linie',
          'timetable' => 'fap_pdf',
        );

        foreach ($sort as $sort_field => $sort_direction) {
          $result = $sort_direction * strcmp($a->{$field_name_map[$sort_field]}, $b->{$field_name_map[$sort_field]});
          if ($result != 0) {
            return $result;
          }
        }

        return 0;
      }
    );
  }

  $total_items = count($timetable_data);

  // Webservice has no support for pagination. Lets simulate it programatically.
  if (isset($limit)) {
    $start = $page * $limit;
    $timetable_data = array_slice($timetable_data, $start, $limit);
  }

  // Convert to desired form.
  $timetable_data = array_map(
    function ($item) {
      return (object) array(
        'nr' => $item->fap_nr,
        'name' => str_replace(array('&lt;br/&gt;', '<br/>'), ' - ', $item->fap_linie),
        'timetable' => $item->fap_pdf,
      );
    },
    $timetable_data
  );

  return array(
    'total_items' => $total_items,
    'data' => $timetable_data,
  );
}
