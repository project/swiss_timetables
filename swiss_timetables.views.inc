<?php

/**
 * @file
 * Views-related hooks.
 */

/**
 * Implements hook_views_plugins().
 */
function swiss_timetables_views_plugins() {
  $plugin = array();
  $plugin['query']['swiss_timetables_plugin_query'] = array(
    'title' => t('Swiss timetables'),
    'help' => t('Swiss timetable query object.'),
    'handler' => 'swiss_timetables_plugin_query',
  );
  return $plugin;
}

/**
 * Implements hook_views_data().
 */
function swiss_timetables_views_data() {
  $data = array();

  // Base data.
  $data['swiss_timetables']['table']['group']  = t('Swiss timetables');
  $data['swiss_timetables']['table']['base'] = array(
    'title' => t('Swiss timetables'),
    'help' => t('Query Swiss timetables.'),
    'query class' => 'swiss_timetables_plugin_query',
  );

  $data['swiss_timetables']['nr'] = array(
    'title' => t('Line nr.'),
    'help' => t('The number of the line.'),
    'field' => array(
      'handler' => 'swiss_timetables_handler_field',
    ),
    'sort' => array(
      'handler' => 'swiss_timetables_handler_sort',
    ),
  );

  $data['swiss_timetables']['name'] = array(
    'title' => t('Line name'),
    'help' => t('The name of the line.'),
    'field' => array(
      'handler' => 'swiss_timetables_handler_field',
    ),
    'filter' => array(
      'handler' => 'swiss_timetables_handler_filter',
    ),
    'sort' => array(
      'handler' => 'swiss_timetables_handler_sort',
    ),
  );

  $data['swiss_timetables']['timetable'] = array(
    'title' => t('Timetable'),
    'help' => t('URL to timetable PDF.'),
    'field' => array(
      'handler' => 'swiss_timetables_handler_field',
    ),
    'sort' => array(
      'handler' => 'swiss_timetables_handler_sort',
    ),
  );

  return $data;
}
