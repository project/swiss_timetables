<?php

/**
 * @file
 * Functions related to autocomplete part of the module.
 */

/**
 * Gets JSON object containing autocomplete suggestions for search keywords.
 */
function swiss_timetables_autocomplete($string = '') {
  $escaped_string = db_like($string);
  $query = db_select('swiss_timetables_autocomplete')
    ->fields('swiss_timetables_autocomplete', array('keyword'))
    ->condition('keyword', $escaped_string . '%', 'LIKE')
    ->range(0, 15);
  // Order by relevance.
  $alias = $query->addExpression('(CASE WHEN `keyword` = :exact THEN 0
         WHEN `keyword` LIKE :search THEN LENGTH(TRIM(BOTH :exact FROM `keyword`))
         ELSE 999
        END)',
    'order_field',
    array(
      ':exact' => $escaped_string,
      ':search' => $escaped_string . '%',
    ));
  $query->orderBy($alias, 'ASC');

  $words = $query->execute()->fetchAll();
  $result = array();
  foreach ($words as $word) {
    $result[$word->keyword] = check_plain($word->keyword);
  }

  return $result;
}
