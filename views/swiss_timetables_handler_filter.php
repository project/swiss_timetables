<?php

/**
 * @file
 * Swiss timetables views filter handler.
 */

/**
 * Views filter handler for fields in Swiss timetables.
 */
class swiss_timetables_handler_filter extends views_handler_filter {

  /**
   * List of supported operators.
   *
   * This kind of construct makes it relatively easy for a child class
   * to add or remove functionality by overriding this function and
   * adding/removing items from this array.
   */
  public function operators() {
    $operators = array(
      'contains' => array(
        'title' => t('Contains'),
        'short' => t('contains'),
        'method' => 'op_contains',
        'values' => 1,
      ),
    );

    return $operators;
  }

  /**
   * Build strings from the operators() for 'select' options.
   */
  public function operator_options($which = 'title') {
    $options = array();
    foreach ($this->operators() as $id => $info) {
      $options[$id] = $info[$which];
    }

    return $options;
  }

  /**
   * Bulds array of operators with values.
   */
  public function operator_values($values = 1) {
    $options = array();
    foreach ($this->operators() as $id => $info) {
      if (isset($info['values']) && $info['values'] == $values) {
        $options[] = $id;
      }
    }

    return $options;
  }

  /**
   * Build admin summary to be displayed in UI.
   */
  public function admin_summary() {
    if (!empty($this->options['exposed'])) {
      return t('exposed');
    }

    $options = $this->operator_options('short');
    $output = '';
    if (!empty($options[$this->operator])) {
      $output = check_plain($options[$this->operator]);
    }
    if (in_array($this->operator, $this->operator_values(1))) {
      $output .= ' ' . check_plain($this->value);
    }
    return $output;
  }

  /**
   * Provide a simple textfield.
   */
  public function value_form(&$form, &$form_state) {
    $form['value'] = array(
      '#type' => 'textfield',
      '#title' => t('Value'),
      '#size' => 30,
      '#default_value' => $this->value,
      '#autocomplete_path' => 'js/swiss_timetables/autocomplete',
      '#js_callback' => array('swiss_timetables' => 'autocomplete'),
    );

    if (!empty($form_state['exposed']) && !isset($form_state['input'][$this->options['expose']['identifier']])) {
      $form_state['input'][$this->options['expose']['identifier']] = $this->value;
    }

  }

  /**
   * Query function.
   *
   * We don't do much here as we do everything in execute() part of the query.
   */
  public function query() {
    $this->field_alias = $this->real_field;
  }

  /**
   * Gets keyword to be used as filter.
   */
  public function keyword() {
    $info = $this->operators();
    if (!empty($info[$this->operator]['method'])) {
      return $this->{$info[$this->operator]['method']}();
    }
  }

  /**
   * Function for "contains" operator.
   */
  public function op_contains() {
    if (empty($this->value[0])) {
      return '*';
    }

    // Looks like the latest version of the service handles numeric values
    // differently and doesn't like wildcards.
    if (is_numeric($this->value[0])) {
      return $this->value[0];
    }

    return '*' . $this->value[0] . '*';
  }

}
