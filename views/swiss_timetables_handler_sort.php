<?php

/**
 * @file
 * Swiss timetables views sort handler.
 */

/**
 * Views sort handler for fields in Swiss timetables.
 */
class swiss_timetables_handler_sort extends views_handler_sort {
  /**
   * Handles query.
   *
   * We don't do much here since we implement most logic in execute() part.
   */
  public function query() {
    $this->field_alias = $this->real_field;
  }
}
