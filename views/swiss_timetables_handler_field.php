<?php

/**
 * @file
 * Swiss timetables views field handler.
 */

/**
 * Views field handler for fields in Swiss timetables.
 */
class swiss_timetables_handler_field extends views_handler_field {
  /**
   * Handles query.
   *
   * We don't do much here since we implement most logic in execute() part.
   */
  public function query() {
    $this->field_alias = $this->real_field;
  }

  /**
   * Render the field.
   */
  function render($values) {
    $value = $this->get_value($values);
    // Keep markup returned by the service but try to ensure it isn't evil.
    return $this->sanitize_value($value, 'xss');
  }
}
