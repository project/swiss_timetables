<?php

/**
 * @file
 * Swiss timetables views query plugin.
 */

/**
 * Views query plugin for the Swiss timetables.
 */
class swiss_timetables_plugin_query extends views_plugin_query {
  /**
   * Handles query.
   *
   * We don't do much here since we implement most logic in execute() part.
   */
  public function query($get_count = FALSE) {}

  /**
   * Request to webservice happens here.
   *
   * We process sort, pagination and exposed filters information before request
   * execution.
   */
  public function execute(&$view) {
    // Handle any sorts.
    $sort = array();
    if (!empty($view->sort)) {
      foreach ($view->sort as $sort_field) {
        $sort[$sort_field->real_field] = $sort_field->options['order'] == 'ASC' ? SWISS_TIMETABLES_SORT_ASC : SWISS_TIMETABLES_SORT_DESC;
      }
    }

    // Pagination.
    $view->init_pager();
    if ($this->pager->use_pager()) {
      $this->pager->set_current_page($view->current_page);
    }

    // Filter. We only support filtering on name.
    $keyword = '*';
    if (!empty($view->filter['name'])) {
      $keyword = $view->filter['name']->keyword();
    }

    $result = swiss_timetables_search($keyword, $this->pager->options['items_per_page'], $this->pager->current_page, $sort);
    if (!empty($result['data'])) {
      $view->result = $result['data'];
    }
    $this->pager->total_items = 0;
    if (!empty($result['total_items'])) {
      $view->total_items = $result['total_items'];
    }

    $this->pager->update_page_info();
  }
}
