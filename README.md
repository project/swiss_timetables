# Swiss official timetable

This module provides integration with a webservice that provides public 
transportation timetables data in Switzerland.

Module provides basic API. Results can be optionally cached to improve end-user
performance. There are two configuration variables that allow users to modify
cache behaviour. 

It also provides Views query plugin that can be used to build nice listings
based on the data provided by the webservice.

There is a pre-configured view that displays timetables in a table and exposes
a filter to search them.

Get in touch with the contact listed on the [Imprint](http://www.fahrplanfelder.ch/en/impressum/)
to get access to the webservice endpoint.


## Configuration

Define the service endpoint to use

`
  $conf['swiss_timetables_endpoint'] = 'http://xxx.fahrplanfelder.ch/webservice.php';
`

Caching is disabled with

`
  $conf['swiss_timetables_cache'] = FALSE;
`

Cache lifetime in seconds (defaults to 1 hour) can be tweaked with

`
  $conf['swiss_timetables_lifetime'] = 86400; 
`

## Maintainers
- [Janez Urevc (@slashrsm)](http://drupal.org/user/744628)

Initial devlopment sponsored by [Cando image](http://www.cando-image.com). 
